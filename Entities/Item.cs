using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Design;
using System.ComponentModel.DataAnnotations.Schema;
namespace tech_test_payment_api.Entities
{
    public class Item
    {
        public int Id { get; set; }     
        public int IdVenda { get; set; }
       
        public string Nome { get; set; }

        public string Descricao{get; set;}

        public int Quantidade { get; set; }

  


    }
}