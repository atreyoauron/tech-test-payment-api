using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Design;
using System.ComponentModel.DataAnnotations.Schema;

namespace tech_test_payment_api.Entities
{
    public class Venda
    {

        public int Id { get; set; }
        public DateTime Data { get; set;}
        public EnumStatusVenda Status { get; set; }
          public Vendedor IdVendedor { get; set; }
        // cria relação com a outra tabela
        public List<Item> Itens {get; set;}

    }
}