using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Entities;
using tech_test_payment_api.Context;
using Swashbuckle.AspNetCore.Annotations;


namespace tech_test_payment_api.Controllers
{
   
    [ApiController]
    [Route("api-docs/[controller]")]

    public class VendaController : ControllerBase
    {
        public readonly LojaContext _context;

        public VendaController(LojaContext context)
        {

            this._context = context;

        }
        [HttpPost("RegistrarVenda")]
        public IActionResult Registrar(Venda venda)
        {

            venda.Status = EnumStatusVenda.Aguardando_Pagamento;
            _context.Vendas.Add(venda);
            _context.SaveChanges();

            return Created("Criado ", venda);



        }

        [HttpGet("BuscarVendaId{id}")]
        public IActionResult BuscarVendaId(int id)
        {

            var venda = _context.Vendas.Find(id);

            if (venda is null) return BadRequest(new { Erro = "Conteudo inexistente" });

            return Ok(venda);
        }

        [HttpPut("AtualizarStatusVenda{id}")]
        public IActionResult AtualizarVenda(EnumStatusVenda em, int id)
        {

            var venda = _context.Vendas.Find(id);


            if (venda is null)

                return NotFound();


            if (venda.Status == EnumStatusVenda.Aguardando_Pagamento)
            {

                if (em != EnumStatusVenda.Pagamento_Aprovado && em != EnumStatusVenda.Cancelada)
                {


                    return BadRequest(new { Erro = "Só é possivel atualizar esse pedido para Pagamento aprovado ou Cancelado" });
                }
            }
            else if (venda.Status == EnumStatusVenda.Pagamento_Aprovado)
            {

                if (em != EnumStatusVenda.Enviado_pela_Transportadora && em != EnumStatusVenda.Cancelada)
                {
                    return BadRequest(new { Erro = "Só é possivel atualizar esse pedido para Enviado ou Cancelado" });
                }
            }
            else if (venda.Status == EnumStatusVenda.Enviado_pela_Transportadora)
            {

                if (em != EnumStatusVenda.Entregue)
                {

                    return BadRequest(new { Erro = "Só é possivel atualizar esse pedido para Entregue" });
                }


            }

            venda.Status = em;
            _context.Vendas.Update(venda);
            _context.SaveChanges();

            var update = em.ToString().Replace('_', ' ');


            return Ok($"Pedido atualizado com sucesso para:{update}");
        }

    }
}