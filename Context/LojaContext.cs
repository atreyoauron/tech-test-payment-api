using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Entities;

namespace tech_test_payment_api.Context
{
    public class LojaContext : DbContext
    {
        public LojaContext(DbContextOptions<LojaContext> options) : base(options)
        {

        }

        public DbSet<Vendedor> Vendedores { get; set; }

        public DbSet<Venda> Vendas { get; set; }

        public DbSet<Item> Itens { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {

            builder.Entity<Venda>(table =>
            {
                table.HasKey(e => e.Id);
                table
                    .HasMany(e => e.Itens)
                    .WithOne()
                    .HasForeignKey(c => c.IdVenda);
            });

            builder.Entity<Item>(table =>
            {
                table.HasKey(e => e.Id);
            });


        }
    }
}