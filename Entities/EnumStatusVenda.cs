using Newtonsoft.Json;
using System.Runtime.Serialization;

namespace tech_test_payment_api.Entities
{

    public enum EnumStatusVenda
    {
        
        Aguardando_Pagamento,

        Pagamento_Aprovado,

        Enviado_pela_Transportadora,
        Entregue,
        Cancelada

    }
}